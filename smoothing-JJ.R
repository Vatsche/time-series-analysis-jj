#################
#### loading 
#################
data <- AirPassengers
#################
#### smoothing
#################
calculate <- function(data){
result <- NULL
for (i in 1:length(data)){
  if (i < frequency(data)/2 | i > length(data)-frequency(data)/2){
    result <- c(result, data[i])
  }else{
  start <- i - frequency(data)/2
  bet_1 <- i - frequency(data)/2 + 1
  bet_2 <- i + frequency(data)/2 - 1
  end <-   i + frequency(data)/2
  
  smoothing <- mean(c((data[start]/2), data[bet_1:bet_2], (data[end])/2))
  result <- c(result, smoothing)
   }
  }
result.ts <- ts(result, start = start(data), end = end(data), freq = frequency(data))
return(result.ts)
}
#################
#### guess S_t
#################
calculate2 <- function(data){
  s_roh <- aggregate(data) / frequency(data)
  c <- 1/frequency(data) * (sum(s_roh))
  result <- ts(s_roh - c, start = start(data), end = end(data), freq = frequency(data))
  return(result)
}
#################
#### results
#################
niveau <- calculate(data)
season <- calculate2(data)
residual <- data - niveau - season
#################
#### plotting data
#################
layout(1:4)
plot (data, main="Raw Data", xlab="Year", ylab="Passengers")
plot(niveau, main="Smoothed Data", xlab="Year", ylab="Passengers")
plot(season, main="Guessed S_t", xlab="Year", ylab="Passengers")
plot(residual, main="Rest", xlab="Year", ylab="Passengers")
#################
#### plotting Time Series
#################
plot(data, main='Time Series Analysis', xlab='Year', ylab='Passengers')
lines(niveau, col='red')
lines(season, col='blue')
legend(x="topright", legend=c("Niveau", "Seasonal Effect", "Raw Data") ,lty=1, col=c('red', 'blue', 'black'), bty='n', cex=.75)



